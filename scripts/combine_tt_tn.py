#!/usr/bin/env python

# ATLAS BSM Combination (13 TeV)

import ROOT as r
r.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem

try:
    gSystem.Load("lib/libCombinationTool.so")
except:
    print ("Could not load library. Make sure that it was compiled correctly.")
# Define
combined = r.CombinedMeasurement("combined_master")
tt_200 = r.Measurement("tt_200")

#f = r.TFile.Open("workspaces/mssm-final/20170710-mssm.13July_final_tcr_tt_Htautau_13TeV_13July_final_tcr_tt_Systs_200/combined/200.root")
#ws = f.Get("combined")
#ws.Print()

tt_200.SetSnapshotName("snapshot_paramsVals_initial")
tt_200.SetFileName("workspaces/mssm-final/20170710-mssm.13July_final_tcr_tt_Htautau_13TeV_13July_final_tcr_tt_Systs_200/combined/200.root")
tt_200.SetWorkspaceName("combined")
tt_200.SetModelConfigName("ModelConfig")
tt_200.SetDataName("obsData")
combined.AddMeasurement(tt_200)


tn_200 = r.Measurement("tn_200")
tn_200.SetSnapshotName("snapshot_paramsVals_initial")
tn_200.SetFileName("workspaces/mssm-final/20170710-mssm.13July_final_tcr_tt_Htautau_13TeV_13July_final_tcr_tt_Systs_200/combined/200.root")
tn_200.SetWorkspaceName("combined")
tn_200.SetModelConfigName("ModelConfig")
tn_200.SetDataName("asimovData")
combined.AddMeasurement(tn_200)
correlation = r.CorrelationScheme("CorrelationScheme")


correlation.SetParametersOfInterest("mu")
correlation.CorrelateParameter("tt_200::SigXsecOverSM,tn_200::SigXsecOverSM", "mu")

correlation.CorrelateParameter("tt_200::ATLAS_norm_Norm_ggH,tt_200::ATLAS_norm_Norm_bbH,tn_200::ATLAS_norm_Norm_Hplus","ATLAS_Sig_XS[1.0,0.0,1.0]")
combined.SetCorrelationScheme(correlation)
combined.CollectMeasurements()
combined.CombineMeasurements()

combined.MakeAsimovData(r.kTRUE, r.CombinedMeasurement.background, r.CombinedMeasurement.background)


# Save the combined workspace
combined.writeToFile("workspaces/combined.root")

# Print useful information like the correlation scheme, re-namings, etc.
combined.Print()
