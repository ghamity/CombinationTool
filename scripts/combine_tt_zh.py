#!/usr/bin/env python

# ATLAS BSM Combination (13 TeV)

import ROOT as r
r.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem

try:
    gSystem.Load("lib/libCombinationTool.so")
except:
    print ("Could not load library. Make sure that it was compiled correctly.")
# Define
combined = r.CombinedMeasurement("combined_master")
tt_300 = r.Measurement("tt_300")

#f = r.TFile.Open("workspaces/mssm-final/20170710-mssm.13July_final_tcr_tt_Htautau_13TeV_13July_final_tcr_tt_Systs_300/combined/300.root")
#ws = f.Get("combined")
#ws.Print()

tt_300.SetSnapshotName("snapshot_paramsVals_initial")
tt_300.SetFileName("workspaces/mssm-final/20170710-mssm.13July_final_tcr_tt_Htautau_13TeV_13July_final_tcr_tt_Systs_300/combined/300.root")
tt_300.SetWorkspaceName("combined")
tt_300.SetModelConfigName("ModelConfig")
tt_300.SetDataName("obsData")
combined.AddMeasurement(tt_300)


zh_300 = r.Measurement("zh_300")
zh_300.SetSnapshotName("snapshot_paramsVals_initial")
zh_300.SetFileName("workspaces/AZh/scan_AZh_13TeV_gridmA_02_hasMerged_True_hasRes_True_300_05/workspaces/AZh_02lep_240517.gridmA_scan_AZh_13TeV_gridmA_02_hasMerged_True_hasRes_True_300_05/combined/300.root")
zh_300.SetWorkspaceName("combined")
zh_300.SetModelConfigName("ModelConfig")
zh_300.SetDataName("asimovData")
combined.AddMeasurement(zh_300)
correlation = r.CorrelationScheme("CorrelationScheme")


correlation.SetParametersOfInterest("mu")
correlation.CorrelateParameter("tt_300::SigXsecOverSM,zh_300::SigXsecOverSM", "mu")

correlation.CorrelateParameter("tt_300::ATLAS_norm_Norm_ggH,tt_300::ATLAS_norm_Norm_bbH,zh_300::ATLAS_norm_bbAfracInv,zh_300::ATLAS_norm_bbAfrac","ATLAS_Sig_XS[0.5,0.5,0.5,0.5]")
combined.SetCorrelationScheme(correlation)
combined.CollectMeasurements()
combined.CombineMeasurements()

combined.MakeAsimovData(r.kTRUE, r.CombinedMeasurement.background, r.CombinedMeasurement.background)


# Save the combined workspace
combined.writeToFile("workspaces/combined.root")

# Print useful information like the correlation scheme, re-namings, etc.
combined.Print()
